// Manages the Job System for the player (a.k.a. 職業)
// NOTE: I think we should do this later. Making enemies and combat is more important for now.

class PlayerJobManager {
    constructor(player) {
        this.player = player;

        this.jobs = ['NEWBIE', 'WARRIOR', 'ARCHER', 'MAGE', 'KNIGHT'];
        // Warrior — melee, high attack
        // Archer — ranged, high dexterity
        // Mage — ranged AOE, high magic
        // Knight — melee, high defense
    }
}