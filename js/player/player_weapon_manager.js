// MANAGES THE PLAYER CHARACTER'S WEAPONS

class PlayerWeaponManager {
    constructor(player) {
        this.player = player;
        this.data = game.cache.getJSON("playerWeaponData");
        this.cooldown = 0;  // for not triggering release weapon in every frame;

        this.weaponAll = {
            weapon0: this.setupWeapon("weapon0", 5, "weapon0"),
            weapon1: this.setupWeapon("weapon1", 5, "weapon1")
        };

        this.weaponHitEffect = {
            weapon0: null,
            weapon1: this.setupWeaponHitEffect(15, "weapon1-hit")
        };

        // only the weapons in this set should be checked collision with enemy, code is now at enemy.js
        this.liveInstance = new Set();
    }

    setupWeapon(dataName, quantity, spriteName) {
        let data = this.data[dataName];
        let weaponGroup = game.add.physicsGroup();
        weaponGroup.enableBody = true;
        weaponGroup.physicsBodyType = Phaser.Physics.ARCADE;
        weaponGroup.createMultiple(quantity, spriteName);
        weaponGroup.setAll('anchor.x', 0.5);
        weaponGroup.setAll('anchor.y', 0.5);
        weaponGroup.setAll('outOfBoundsKill', true);
        weaponGroup.setAll('checkWorldBounds', true);

        // set properties of every weapon object
        weaponGroup.forEach(function (weapon) {
            weapon._data = {};
            weapon._data.damage = data.damage;
            if (typeof data.speed === "number") {
                weapon._data.speed = data.speed;
            } else if (data.speed === "groundSpeed") {
                weapon._data.speed = -playState.groundManager.groundSpeed;
            }
            weapon._data.range = data.range;  // currently not used
            weapon._data.enemySuffered = new Set();
            weapon._data.name = dataName;

            // when a weapon is killed (out of world bound, lifespan is end, etc...)
            // 1. we update the living instance set in weaponManager 
            // 2. something like double linked list, 
            //    we delete the suffered enemys' suffered weapon set entry of this weapon,
            //    and clear weapon itself's suffered enemy set,
            //    so only when the next time this weapon is relifed by getFirstDead() & reset(), it can cause damage on the enemy again
            weapon.events.onKilled.add(function (killedWeapon) {
                this.liveInstance.delete(killedWeapon);
                killedWeapon._data.enemySuffered.forEach((enemy) => {
                    enemy.weaponSuffered.delete(killedWeapon);
                });
                weapon._data.enemySuffered.clear();
            }, this);

            if (dataName === "weapon0") {
                weapon.animations.add("stand", [0, 1, 2, 3, 4], 8, false);
            } else if (dataName === "weapon1") {
                weapon.animations.add("stand", [0, 1, 2], 8, true);
            }

        }, this);

        return weaponGroup;
    }

    setupWeaponHitEffect(quantity, spriteName) {
        let weaponHitEffectGroup = game.add.physicsGroup();  // merely to enable to use body.velocity
        weaponHitEffectGroup.createMultiple(quantity, spriteName);
        weaponHitEffectGroup.setAll('anchor.x', 0.5);
        weaponHitEffectGroup.setAll('anchor.y', 0.5);
        weaponHitEffectGroup.forEach(function (weapon) {
            if (spriteName === "weapon1-hit") {
                weapon.animations.add("stand", [0, 1, 2, 3, 4, 5, 6], 8, false);
            }

        }, this);

        return weaponHitEffectGroup;
    }

    release(type, target) {
        let weapon = this.weaponAll[type].getFirstDead();
        if (!weapon) return;

        if (type === "weapon0") {
            // weapon.reset(this.player.sprite.x + 80, this.player.sprite.y);
            if (!target) return;
            weapon.reset(target.x, target.y);
            // TODO: (bug)
            // if a weapon end it's life without it's animation being completed
            // (case 1. if lifespan is shorter than the time it needs to run the entire animation)
            // (case 2. or if weapon goes out of world bound, which then call kill()),
            // then the next time it is selected by getFirstDead() and reset(), animation.play() will not work
            // case 1 can be solved by giving an enough lifespan
            // case 2 hasn't been solved yet
            weapon.lifespan = 800;
        } else if (type === "weapon1") {
            weapon.reset(this.player.sprite.x + 70, this.player.sprite.y + 20);
            weapon.lifespan = 900;
        }

        weapon.animations.play("stand");
        weapon.body.velocity.x = weapon._data.speed;

        this.liveInstance.add(weapon);

    }

    hitEffect(type, weapon, enemy) {
        if (!this.weaponHitEffect[type]) return;
        let effect = this.weaponHitEffect[type].getFirstDead();
        if (!effect) return;
        if (type === "weapon1") {
            effect.reset(weapon.x, weapon.y);
            effect.lifespan = 1000;
            effect.body.velocity.x = enemy.body.velocity.x;
        }
        effect.animations.play("stand");
    }

    // TODO: return false if the closest enemy(target) is not in the weapon(type)'s range
    checkRange(type, target) {
        return true;
    }

    update() {
        this.cooldown = Math.max(this.cooldown - 1, 0);
    }
}

