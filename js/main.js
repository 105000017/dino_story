// GLOBAL CONSTANTS FOR GETTING INFO
const GAME_WIDTH = 900; // game screen width
const GAME_HEIGHT = 600; // game screen height

let game;

window.onload = function () {

    let conf = {
        width: GAME_WIDTH,
        height: GAME_HEIGHT,
        renderer: Phaser.AUTO,
        antialias: false,
        parent: 'game-container'
    };

    game = new Phaser.Game(conf);

    game.state.add('play', playState);
    game.state.add('load', loadState);
    game.state.start('load');
};
