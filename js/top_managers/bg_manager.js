// CONTROLS THE BACKGROUND IMAGES, WHICH SHOULDN'T HAVE IMPACT ON ANYTHING ELSE

class BgManager {
    constructor() {
        this.bgGradient = game.add.sprite(0, 0, 'backgroundGradient');

        this.cloudSpeed = -50;
        this.cloudTime = 0;
        this.cloudInterval = 12000;
        this.cloudIndex = 0;

        this.cloudGroup = game.add.physicsGroup();
        this.cloudGroup.createMultiple(1, ['cloud4', 'cloud1', 'cloud2', 'cloud3', 'cloud5']);
        this.cloudGroup.setAll('outOfBoundsKill', true);
        this.cloudGroup.setAll('checkWorldBounds', true);

        this.farBgSpeed = -70;
        this.farBgTime = 7000;
        this.farBgInterval = 15000;
        this.farBgIndex = 0;

        this.farBgGroup = game.add.physicsGroup();
        this.farBgGroup.createMultiple(1, ['farBg1', 'farBg2']);
        this.farBgGroup.setAll('anchor.y', 1);
        this.farBgGroup.setAll('outOfBoundsKill', true);
        this.farBgGroup.setAll('checkWorldBounds', true);

        this.nearBgSpeed = -130;
        this.nearBgTime = 0;
        this.nearBgInterval = 6000;
        this.nearBgIndex = 0;

        this.nearBgGroup = game.add.physicsGroup();
        this.nearBgGroup.createMultiple(1, ['nearBg1', 'nearBg2', 'nearBg3', 'nearBg4']);
        this.nearBgGroup.setAll('anchor.y', 1);
        this.nearBgGroup.setAll('outOfBoundsKill', true);
        this.nearBgGroup.setAll('checkWorldBounds', true);
    }

    update() {
        if (playState.stateTag === STATES.RUNNING) {
            // Checks for gameOver condition to prevent a bug that makes the background disappear at the game over screen
            if (game.time.now > this.cloudTime) {
                let cloud = this.cloudGroup.getChildAt(this.cloudIndex);
                this.cloudIndex = (this.cloudIndex + 1) % 5;
                if (cloud) {
                    cloud.reset(GAME_WIDTH, game.rnd.between(30, 300));
                    cloud.body.velocity.x = this.cloudSpeed;
                }
                this.cloudTime = game.time.now + this.cloudInterval;
            }

            if (game.time.now > this.farBgTime) {
                let farBg = this.farBgGroup.getChildAt(this.farBgIndex);
                this.farBgIndex = (this.farBgIndex + 1) % 2;
                if (farBg) {
                    farBg.reset(GAME_WIDTH, GAME_HEIGHT - 80);
                    farBg.body.velocity.x = this.farBgSpeed;
                }
                this.farBgTime = game.time.now + this.farBgInterval;
            }

            if (game.time.now > this.nearBgTime) {
                let nearBg = this.nearBgGroup.getChildAt(this.nearBgIndex);
                this.nearBgIndex = (this.nearBgIndex + 1) % 4;
                if (nearBg) {
                    nearBg.reset(GAME_WIDTH, GAME_HEIGHT - 80);
                    nearBg.body.velocity.x = this.nearBgSpeed;
                }
                this.nearBgTime = game.time.now + this.nearBgInterval;
            }
        }
    }
}