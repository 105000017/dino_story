// Holds and controls all items
class ItemManager {
    constructor() {
        this.redPotion = new RedPotion(this);
        this.orangePotion = new OrangePotion(this);
        this.bluePotion = new BluePotion(this);
        this.sonicRing = new SonicRing(this);
        this.rareCandy = new RareCandy(this);
        // TODO: ^^^ add new items here ^^^

        this.allItems = [
            this.redPotion,
            this.orangePotion,
            this.bluePotion,
            this.sonicRing,
            this.rareCandy
            // TODO: ^^^ add new items here ^^^
        ];
    }

    update() {
        this.allItems.forEach(function(item){
            item.update();
        });
    }
}