// Controls all enemies, spawns all enemies, detects enemy collisions
class EnemyManager {
    constructor() {
        this.enemySpawnData = game.cache.getJSON('enemySpawnData');

        this.greenSnail = new GreenSnail();
        this.blueSnail = new BlueSnail();
        this.redSnail = new RedSnail();
        this.orangeMushroom = new OrangeMushroom();
        // TODO: ^^^ add new enemies here ^^^

        this.allEnemies = [
            this.greenSnail,
            this.blueSnail,
            this.redSnail,
            this.orangeMushroom
            // TODO: ^^^ add new enemies here ^^^
        ];

        this.enemySpawnTimers = [];
    }

    updateLevel(currLvl) {
        // remove timers for spawning enemies of previous level
        this.enemySpawnTimers.forEach(function (timer) {
            game.time.events.remove(timer);
        });
        this.enemySpawnTimers = [];
        // get spawn data for current player level
        let currSpawnData = this.enemySpawnData[currLvl];
        // set event timers for spawning enemies
        Object.keys(currSpawnData).forEach(function (enemy) {
            const enemySpawnInterval = currSpawnData[enemy];
            const enemySpawnTimer = game.time.events.loop(Phaser.Timer.SECOND * enemySpawnInterval, this.spawnEnemyType, this, enemy);
            this.enemySpawnTimers.push(enemySpawnTimer);
            console.log("1 " + enemy + " per " + currSpawnData[enemy] + " second(s)");
        }, this);
    }

    spawnEnemyType(enemyType) {
        switch (enemyType) {
            case 'Green Snail':
                this.greenSnail.spawn();
                break;
            case 'Blue Snail':
                this.blueSnail.spawn();
                break;
            case 'Red Snail':
                this.redSnail.spawn();
                break;
            case 'Orange Mushroom':
                this.orangeMushroom.spawn();
                break;
            // TODO: ^^^ ADD NEW ENEMIES HERE ^^^
            default:
                console.log("No enemy type of " + enemyType + " found.");
        }
    }

    update() {
        this.allEnemies.forEach(function(enemy){
            enemy.update();
        });
    }
}