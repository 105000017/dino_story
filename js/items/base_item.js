class BaseItem {
    constructor(manager) {
        this.manager = manager;
        this.items = game.add.group();
        this.spawnLock = false; // this will be overwritten by child classes that need to spawn-lock until a quest is active
    }

    setupItem(quantity, spriteName) {
        this.items.enableBody = true;
        this.items.physicsBodyType = Phaser.Physics.ARCADE;
        this.items.createMultiple(quantity, spriteName);
        this.items.setAll('anchor.x', 0);
        this.items.setAll('anchor.y', 1);
        this.items.setAll('outOfBoundsKill', true);
        this.items.setAll('checkWorldBounds', true);
        this.items.forEach(function (item) {
            item.tween = game.add.tween(item).to({y: GAME_HEIGHT - 90}, 500, Phaser.Easing.Cubic.Out, false, 0, -1, true);
        }, this);
    }

    spawn(x) {
        const item = this.items.getFirstExists(false);
        if (item) {
            item.reset(x, GAME_HEIGHT - 80);
            item.tween.start();
            item.body.velocity.x = -playState.groundManager.groundSpeed;
        }
    }

    // For a 30% chance to spawn item, call chanceToSpawn(30)
    chanceToSpawn(percent, x) {
        if (this.spawnLock) {
            // stops items from spawning if they are quest items and the quest isn't active
            return;
        }
        if (Phaser.Utils.chanceRoll(percent)) {
            this.spawn(x + game.rnd.between(-10, 10));
        }
    }

    update() {
        // Check collisions with playState.player
        game.physics.arcade.overlap(playState.player.sprite, this.items, function (player, item) {
            this.itemEffect(player);
            item.kill();
        }, null, this);

        // Check collisions with Dino's Child
        game.physics.arcade.overlap(playState.dinosChild.sprite, this.items, function (dinosChild, item) {
            this.itemEffectOnChild(dinosChild);
            item.kill();
        }, null, this);
    }
}