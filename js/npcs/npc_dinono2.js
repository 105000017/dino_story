class NPCDinono2 extends BaseNPC {
    constructor(manager) {
        super(manager, 'npcDinono', 'npcDinono2');

        this.questProgress = 0;
        this.questGoal = 0;
        this.questTarget = 'None';
    }

    questReward() {
        // TODO: kill Dino
    }

    trackQuest(questEvent) {
        // Shouldn't need to be used
    }

    // overwrite
    update() {

        // Trigger dialogue when touching player
        game.physics.arcade.overlap(playState.player.sprite, this.sprite, function () {

            // First time meeting, accepting a quest
            if (!this.questAccepted) {
                // This boolean will prevent the overlap from triggering multiple times.
                this.questAccepted = true;
                this.questCompleted = true;
                console.log("Ran into Dinono after killing child. Instant death.");
                // Remove from unlockedNPCs
                let index = this.manager.unlockedNPCs.indexOf(this);
                if (index > -1) {
                    this.manager.unlockedNPCs.splice(index, 1);
                }

                // TODO: NPC QuestGive dialogue
                //  You can use this.spriteName to differentiate which NPC is talking

            }

            // Should trigger immediately after initial dialogue
            if (!this.questEnded && this.questCompleted) {
                this.questEnded = true;
                playState.player.hurt(99999);
            }
        }, null, this);
    }
}