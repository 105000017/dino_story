class BaseNPC {
    constructor(manager, loadSprite, spriteName=loadSprite) {
        this.manager = manager;
        this.loadSprite = loadSprite;
        this.spriteName = spriteName;

        this.sprite = game.add.sprite(GAME_WIDTH, GAME_HEIGHT - 70, this.loadSprite);
        this.sprite.anchor.setTo(0, 1);
        game.physics.enable(this.sprite, Phaser.Physics.ARCADE);
        this.sprite.body.outOfBoundsKill = true;
        this.sprite.body.checkWorldBounds = true;

        this.questAccepted = false;
        this.questCompleted = false;
        this.questEnded = false;
    }

    spawn() {
        this.sprite.reset(GAME_WIDTH, GAME_HEIGHT - 80);
        this.sprite.body.velocity.x = -playState.groundManager.groundSpeed;
    }

    update() {

        // Trigger dialogue when touching player
        game.physics.arcade.overlap(playState.player.sprite, this.sprite, function () {

            // First time meeting, accepting a quest
            if (!this.questAccepted) {
                // This boolean will prevent the overlap from triggering multiple times.
                this.questAccepted = true;
                console.log("Accepted quest: " + this.spriteName);
                // Add to acceptedQuestNPCs
                this.manager.acceptedQuestNPCs.push(this);
                // Remove from unlockedNPCs
                let index = this.manager.unlockedNPCs.indexOf(this);
                if (index > -1) {
                    this.manager.unlockedNPCs.splice(index, 1);
                }
                // Add quest to UI
                playState.uiManager.addActiveQuest(this);
                // Unlock quest item spawn (so enemies can drop the quest item)
                this.manager.unlockItemSpawn(this);

                // TODO: NPC QuestGive dialogue
                //  You can use this.spriteName to differentiate which NPC is talking

                // TODO: npc_dinono2.js overwrites the update function, so also add the callback there
                // TODO: npc_diyesyes.js overwrites the update function, so also add the callback there

            }

            // Second time meeting (should only happen after the quest is completed), handing in a completed quest
            if (!this.questEnded && this.questCompleted) {
                // This boolean will prevent the overlap from triggering multiple times.
                this.questEnded = true;
                console.log("Ended quest: " + this.spriteName);
                // Remove from completedQuestNPCs
                let index = this.manager.completedQuestNPCs.indexOf(this);
                if (index > -1) {
                    this.manager.completedQuestNPCs.splice(index, 1);
                }
                // Remove quest from UI
                playState.uiManager.removeActiveQuest(this);

                // TODO: NPC QuestComplete dialogue
                //  You can use this.spriteName to differentiate which NPC is talking

                this.questReward();
                this.sprite.kill();
            }

        }, null, this);
    }
}