class NPCDiyesyes extends BaseNPC {
    constructor(manager) {
        super(manager, 'npcDiyesyes');

        this.sprite.scale.setTo(0.5, 0.5);

        this.questProgress = 0;
        this.questGoal = 0;
        this.questTarget = 'None';
    }

    trackQuest(questEvent) {
        // Shouldn't need to be used
    }

    // overwrite
    update() {

        // Trigger dialogue when touching player
        game.physics.arcade.overlap(playState.player.sprite, this.sprite, function () {

            // First time meeting, accepting a quest
            if (!this.questAccepted) {
                // This boolean will prevent the overlap from triggering multiple times.
                this.questAccepted = true;
                this.questCompleted = true;
                console.log("Ran into Diyesyes with child.");
                // Remove from unlockedNPCs
                let index = this.manager.unlockedNPCs.indexOf(this);
                if (index > -1) {
                    this.manager.unlockedNPCs.splice(index, 1);
                }

                // TODO: NPC QuestGive dialogue
                //  You can use this.spriteName to differentiate which NPC is talking

            }

            // Should trigger immediately after initial dialogue
            if (!this.questEnded && this.questCompleted) {
                this.questEnded = true;
                if (this.manager.dialogueChoices.npcDiyesyes === 1){
                    playState.dinosChild.sold = true;
                    playState.dinosChild.sprite.kill();
                    playState.player.spitMoney();
                }
            }
        }, null, this);
    }
}