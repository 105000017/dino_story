class enemy {
    constructor() {
        this.enemyData = game.cache.getJSON('enemyData');
        // These variables will be filled by EnemyFactory
        this.atk = 0;
        this.HP = 0;
        this.enemies = game.add.physicsGroup();

    }

    spawn() {
        const enemy = this.enemies.getFirstExists(false);
        if (enemy) {
            enemy.reset(GAME_WIDTH, GAME_HEIGHT - 80);
            enemy.animations.play('normal');
            enemy.body.velocity.x = -playState.groundManager.groundSpeed;
            enemy.HP = this.HP;
        }
    }

    setupEnemy(dataName, quantity, spriteName, enemyClass) {
        const enemyData = this.enemyData[dataName];

        enemyClass.atk = enemyData.atk;
        enemyClass.HP = enemyData.HP;

        enemyClass.enemies.enableBody = true;
        enemyClass.enemies.physicsBodyType = Phaser.Physics.ARCADE;
        enemyClass.enemies.createMultiple(quantity, spriteName);
        enemyClass.enemies.setAll('anchor.x', 0);
        enemyClass.enemies.setAll('anchor.y', 1);
        enemyClass.enemies.setAll('body.maxVelocity.x', playState.groundManager.groundSpeed);
        enemyClass.enemies.setAll('outOfBoundsKill', true);
        enemyClass.enemies.setAll('checkWorldBounds', true);
        enemyClass.enemies.forEach(function (enemy) {
            enemy.EXPDrop = enemyData.EXPDrop;
            enemy.weaponSuffered = new Set();
        });
    }

    hurt(instance, damage) {
        instance.HP -= damage;
        // TODO or Not todo: display damage text
        //  Response: I actually used two separate display damage/heal texts in player.js, maybe we should write a
        //  specialized code for that to simplify things?
    }

    update() {

        // Check collisions with playState.player
        game.physics.arcade.overlap(playState.player.sprite, this.enemies, function () {
            playState.player.hurt(this.atk);
        }, null, this);

        // Check collisions with player's weapon
        playState.player.playerWeaponManager.liveInstance.forEach((weapon) => {
            game.physics.arcade.overlap(weapon, this.enemies, function (weapon, enemy) {
                // check if enemy is hurt by the same weapon
                if (enemy.weaponSuffered.has(weapon)) return;

                // TODO: add weapon genre, and use weapon genre to determine the action when hit
                if (weapon._data.name === "weapon0") {
                    weapon._data.enemySuffered.add(enemy);
                    enemy.weaponSuffered.add(weapon);
                } else if (weapon._data.name === "weapon1") {
                    playState.player.playerWeaponManager.hitEffect("weapon1", weapon, enemy);
                    weapon.kill();
                }
                console.log("enemy: QQ");
                // Note: I added the player's ATK stat to damage
                this.hurt(enemy, weapon._data.damage + playState.player.stats.atk);
            }, null, this);
        });

        // Check collisions with Dino's Child
        game.physics.arcade.overlap(playState.dinosChild.sprite, this.enemies, function () {
            playState.dinosChild.hurt(this.atk);
        }, null, this);

    }
}