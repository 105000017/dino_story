class GreenSnail extends enemy {
    constructor() {
        super();
        this.setupEnemy('Green Snail', 5, 'greenSnail', this);

        this.enemies.forEach(function(enemy){
            // I can't get callAll to work for some reason...
            enemy.body.setSize(35, 25, 0, 10);
        }, this);
        this.enemies.callAll('animations.add', 'animations', 'normal', [0, 1, 2, 3, 4, 3, 2, 1], 12, true);
        this.enemies.callAll('animations.add', 'animations', 'hit', [5], 1, false);
        this.enemies.callAll('animations.add', 'animations', 'die', [5, 6, 7, 8, 9, 10, 11, 12, 13], 12, false);
    }

    update() {
        super.update();

        // Check for death
        this.enemies.forEachAlive(function(enemy){
            if (enemy.HP <= 0) {
                // TODO: play the 'die' animation before killing the enemy sprite
                //  make sure the enemy doesn't hurt the player anymore during this animation
                enemy.weaponSuffered.clear();

                playState.itemManager.redPotion.chanceToSpawn(10, enemy.x);
                playState.itemManager.rareCandy.chanceToSpawn(5, enemy.x);
                playState.itemManager.sonicRing.spawnRingBunch(game.rnd.between(1, 5), enemy.x);

                // Let NPCManager know when an enemy dies for quest-tracking
                playState.npcManager.notifyQuestNPC('Killed Green Snail');

                // Give player EXP
                playState.player.currEXP += enemy.EXPDrop;

                enemy.kill();
            }
        }, this);
    }
}