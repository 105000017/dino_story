class OrangeMushroom extends enemy {
    constructor() {
        super();
        this.setupEnemy('Orange Mushroom', 5, 'orangeMushroom', this);

        this.enemies.forEach(function(enemy){
            // I can't get callAll to work for some reason...
            enemy.body.setSize(40, 50, 0, 10);
        }, this);
        this.enemies.callAll('animations.add', 'animations', 'normal', [0, 1], 2, true);
        this.enemies.callAll('animations.add', 'animations', 'read_to_jump', [2], 1, false);
        this.enemies.callAll('animations.add', 'animations', 'jumping', [3], 1, false);
        this.enemies.callAll('animations.add', 'animations', 'landing', [4], 1, false);
        this.enemies.callAll('animations.add', 'animations', 'hit', [5], 1, false);
        this.enemies.callAll('animations.add', 'animations', 'die', [5, 6, 7, 8], 12, false);
    }

    update() {
        super.update();

        // Check for death
        this.enemies.forEachAlive(function(enemy){
            if (enemy.HP <= 0) {
                // TODO: play the 'die' animation before killing the enemy sprite
                //  make sure the enemy doesn't hurt the player anymore during this animation
                enemy.weaponSuffered.clear();

                playState.itemManager.redPotion.chanceToSpawn(10, enemy.x);
                playState.itemManager.bluePotion.chanceToSpawn(10, enemy.x);
                playState.itemManager.orangePotion.chanceToSpawn(10, enemy.x);
                playState.itemManager.rareCandy.chanceToSpawn(20, enemy.x);
                playState.itemManager.sonicRing.spawnRingBunch(game.rnd.between(1, 20), enemy.x);

                // Let NPCManager know when an enemy dies for quest-tracking
                playState.npcManager.notifyQuestNPC('Killed Orange Mushroom');

                // Give player EXP
                playState.player.currEXP += enemy.EXPDrop;

                enemy.kill();
            }
        }, this);
    }
}