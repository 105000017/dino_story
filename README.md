# Dino Story

### Work Distribution
CORE
- Jimmy:
    - Weapon
        - Add movement-related attack/dodge
        - Finished range detection
        - Add direction detection
    - Enemy (EnemyFactory combine into EnemyManager, Enemy update() inherit from parent class, drop items and EXP)
        - Add jumping for enemy (Orange Mushroom)
        - Falling enemies
        - Change enemy spawn data to spawn high-level enemies at low player levels
- Brian:
    - NPC (Story, Quests, etc)
        - Change into cohesive story
        - Add job changer NPC
        - Add ending NPC
        - Add low-gravity child (when collide with Dino, will jump)
        - Add weird story
        - Choices:
            - Sell child
            - Raise child (evolves)
    - Quests
    - Story
        - Add StoryManager with variables for recording player options
- Chang:
    - Dialogue
        - Dialogue box is in "img/ui/ui_dialogue.png"
        - Dialogue options
- Charlie
    - Stats (Jobs, Player jump height / move speed)
    - Add job-specific skills

EXTRAS
- Jump height based on spacebar hold time
- Simple items (Potions)
- Game modes
- Second-floor ground
- Dino can dodge + enemies that fly
- Endless mode after story ends

### Functions (so far)
- Make Dino move left/right
- Make Dino jump
- Ground moves left and loops ("endless runner")
- Dino has 3 animations: run, jump, and idle
- Shows Dino's HP, MP, level, and job on the top-left corner.
- Shows Dino's current EXP and the required EXP to reach the next level at the bottom.
- Spawns a grave when Dino dies, and allows replaying the game by pressing the spacebar.
- When Dino is hurt, the damage is displayed above, and Dino gains temporary invulnerability for 1 second.
- When Dino is healed, the heal amount is displayed above in a different color.
- NPCs spawn at set intervals, based on player level.
- Quests can be accepted from NPCs by running into them.
- Once quests are finished, NPCs will return, and running into them again will turn in the quest, rewarding EXP.
- Items have a chance to drop from enemies when they die.
- The player can pick up and use the items by running into them. Some will have a particle emitter.